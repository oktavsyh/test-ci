<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Laporan extends CI_Controller {

        public function index()
        {
            $this->load->library('Pdf');
            $this->load->model('Mahasiswa_model');
            $data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();
            $this->load->view('laporan_mahasiswa', $data);
        }
    }
?>