<?php
    $pdf = new Pdf('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor("Oktaviansyah");
    $pdf->SetTitle("Daftar Mahasiswa");
    $pdf->SetSubject("Laporan Daftar Mahasiswa dalam Bentuk PDF");
    $pdf->SetKeywords('TCPDF, PDF, MySQL, Codeigniter');

    // set default header data
    $pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont('helvetica');

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set font
    $pdf->SetFont('helvetica', '', 12);
    
    // ---------------------------------------------------------
    
    
    
    // add a page
    $pdf->AddPage();
    $content = '';
    $content .= '
    <h4 align="center">Daftar Mahasiswa</h4><br />
    <table border="1" cellspacing="0" cellpadding="3">
        <tr>
            <th>NRP</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Jurusan</th>
        </tr>
    ';
    foreach ($mahasiswa as $x) {
        $content .= '<tr>
                        <td>'.$x->nrp.'</td>
                        <td>'.$x->nama.'</td>
                        <td>'.$x->email.'</td>
                        <td>'.$x->jurusan.'</td>
                     </tr>
                     ';
    }
    $content .= '</table>';
    $pdf->writeHTML($content);
    $pdf->Output('file.pdf', 'I');
?>